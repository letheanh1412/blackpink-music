let boolean;
$(".status-dont-know-what-to-do").click(function (){
    if (!boolean) {
        boolean = true;
        $("audio")[0].play();
        $(".status-dont-know-what-to-do").removeClass("fa-play-circle").addClass("fa-pause-circle");
    } else {
        boolean = false;
        $("audio")[0].pause();
        $(".status-dont-know-what-to-do").removeClass("fa-pause-circle").addClass("fa-play-circle")
    }
});

